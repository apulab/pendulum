int inmulseTimeOut = 350;

int buttonPin = 40;
int rel1Pin = 6;
int rel2Pin = 7;
int engEncoderA = 50;
int engEncoderB = 48;
int impulseTimeA = 0;
int impulseTimeB = 0;
int timer = 300;
void setup() {
  // put your setup code here, to run once:
  pinMode(buttonPin, INPUT);
  pinMode(rel1Pin, OUTPUT);
  pinMode(rel2Pin, OUTPUT);
  pinMode(engEncoderA, INPUT);
  pinMode(engEncoderB, INPUT);
  Serial.begin(9600);
}

void loop() {

  digitalWrite(rel1Pin, HIGH);
  digitalWrite(rel2Pin, LOW);
  delay(timer);
  getImpulseLength();
  Serial.print(";___");

  //digitalWrite(rel1Pin, LOW);
  //digitalWrite(rel2Pin, HIGH);
  delay(timer);
  getImpulseLength();
  Serial.println(";");

  timer -= 20;
  if (timer < 20)timer = 300;
}

void getImpulseLength() {
  impulseTimeA = pulseIn(engEncoderA, HIGH, inmulseTimeOut);
  impulseTimeB = pulseIn(engEncoderB, HIGH, inmulseTimeOut);
  Serial.print("Impuls A = ");
  Serial.print(impulseTimeA);
  Serial.print("___");
  Serial.print("Impuls B = ");
  Serial.print(impulseTimeB);
}
