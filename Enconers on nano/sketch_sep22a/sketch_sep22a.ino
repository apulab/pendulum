
//Конфиги
int enc_1_a = 2;
int enc_1_b = 3;
int enc_2_a = 7;
int enc_2_b = 8;
int pTime = 1; // Период опроса энкодеров
int sendDataPeriod = 10; // В количествах циклов pTime
float dAngle = 10; // Угол одного шага энкодера

//Глобальные переменные
float angle_1 = 0;
float angle_2 = 0;
uint8_t previous_code_1 = 0;
uint8_t previous_code_2 = 0;

//Переменные
int dataTimer = 0;

/* Функция декодирования кода Грея, взятая с Википедии.
* Принимает число в коде Грея, возвращает обычное его представление.
*/
//unsigned graydecode(unsigned);
/**
*Обработчики значений энкодеров
*/
//int getDirectionEncoder(int, int, uint8_t &);

void setup()
{
  pinMode(enc_1_a, INPUT);
  pinMode(enc_1_b, INPUT);
  pinMode(enc_2_a, INPUT);
  pinMode(enc_2_b, INPUT);
  Serial.begin(115200);
  Serial.println("Success load");
}

void loop()
{
  int DirectionEncoder_1 = getDirectionEncoder(enc_1_a, enc_1_b, previous_code_1);
  if ( DirectionEncoder_1 == RIGHT) {
    angle_1 += dAngle;
  }
  else if (DirectionEncoder_1 == LEFT) {
    angle_1 -= dAngle;
  }

  int DirectionEncoder_2 = getDirectionEncoder(enc_2_a, enc_2_b, previous_code_2);
  if ( DirectionEncoder_2 == RIGHT) {
    angle_2 += dAngle;
  }
  else if (DirectionEncoder_2 == LEFT) {
    angle_2 -= dAngle;
  }
  dataTimer++;
  if (dataTimer == 100) {
    dataTimer = 0;
    sendData();

  }
  delay(pTime);
}

void sendData() {
  Serial.print(angle_1);
  Serial.print(":");
  Serial.println(angle_2);
}
